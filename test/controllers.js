var assert = require("assert")
describe('Sanity', function(){
  describe('#indexOf()', function(){
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    })
  })
})

describe('JSON is valid', function(){
  describe('is an object and is there', function(){
    it('should have a valid json response', function(){
      assert.equal('object', typeof(data));
    })
  })
})

describe('Response', function(){
  describe('first entry destination should be', function(){
    it('FAIRFIELD METRO', function(){
      assert.equal('FAIRFIELD METRO', data.GetTripStatusJsonResult[0].Destination);
    })
  })
  describe('origin time should be', function(){
    it('6:59 AM', function(){
      assert.equal('6:59 AM', data.GetTripStatusJsonResult[0].OriginTime);
    })
  })
  describe('train status should be', function(){
    it('Departed', function(){
      assert.equal('Departed', data.GetTripStatusJsonResult[0].TrainStatus);
    })
  })
})