module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Tests
    karma: {
      unit: {
        options: {
          files: ['test/**/*.js']
        }
      }
    },

    // Configure a mochaTest task
    mochaTest: {
      test: {
        options: {
          reporter: 'nyan',
          require: [
            //'index.js', // How do I get this in there?!
            function(){ data=require('./test/tripstatus.json'); },
          ]
        },
        src: ['test/**/*.js']
      }
    },

    // Fuglify js files in production
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= pkg.version %> - Built on <%= grunt.template.today("yyyy-mm-dd") %> */\n',
      },
      defaults: {
        files: [{
            expand: true,
            cwd: '',
            src: 'index.js',
            dest: 'min',
            ext: '.min.js'
        }]
      },
    },

    // JSHint in development
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
      },
      default: ['index.js'],             // default hint settings
      strict: {                           // stricter hint settings
        options: {
          curly: false
        },
        files: {
          src: ['index.js']
        },
      }
    },


  });

  // Load plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-mocha-test');

  // Default tasks
  grunt.registerTask('default', ['jshint:default', 'mochaTest']);

  // Development environment tasks
  grunt.registerTask('dev', ['jshint:default']);

  // Production environment tasks
  grunt.registerTask('prod', ['jshint:default', 'uglify']);

};